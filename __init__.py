# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import configuration
from . import carrier
from . import unit_load


def register():
    Pool.register(
        configuration.Configuration,
        configuration.ConfigurationULWeightUOM,
        unit_load.UnitLoad,
        unit_load.UnitLoadWeightStart,
        unit_load.UnitLoadWeightData,
        module='stock_unit_load_weight', type_='model')
    Pool.register(
        unit_load.WeighUL,
        unit_load.WeightULRelate,
        module='stock_unit_load_weight', type_='wizard')
    Pool.register(
        carrier.CMR,
        carrier.RoadTransportNote,
        module='stock_unit_load_weight', type_='report')
    Pool.register(
        unit_load.UnitLoad2,
        module='stock_unit_load_weight', type_='model',
        depends=['stock_move_done2cancel'])
    Pool.register(
        unit_load.UnitLoad3,
        unit_load.UnitLoadWeightStart2,
        module='stock_unit_load_weight', type_='model',
        depends=['carrier_load_ul'])
    Pool.register(
        unit_load.WeighUL2,
        module='stock_unit_load_weight', type_='wizard',
        depends=['carrier_load_ul'])
