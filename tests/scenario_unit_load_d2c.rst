================================
Check unit load weight d2t moves
================================

Imports::

    >>> import datetime
    >>> from trytond.tests.tools import activate_modules
    >>> from proteus import Model, Wizard

from dateutil.relativedelta import relativedelta

    >>> from decimal import Decimal
    >>> from trytond.modules.company.tests.tools import create_company, \
    ... get_company

today = datetime.date.today()
tomorrow = today + relativedelta(days=1)


Install unit load Module::

    >>> config = activate_modules(['stock_unit_load_weight', \
    ... 'stock_move_done2cancel'])


Create company::

    >>> _ = create_company()
    >>> company = get_company()


Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> kg, = ProductUom.find([('name', '=', 'Kilogram')])
    >>> template = ProductTemplate()
    >>> template.name = 'Product'
    >>> template.default_uom = kg
    >>> template.type = 'goods'
    >>> template.list_price = Decimal('20')
    >>> template.save()
    >>> product1, = template.products
    >>> product1.cost_price = Decimal('8')
    >>> product1.save()
    >>> template = ProductTemplate()
    >>> template.name = 'Product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.list_price = Decimal('20')
    >>> template.save()
    >>> product2, = template.products
    >>> product2.cost_price = Decimal('8')
    >>> product2.save()


Get stock locations::

    >>> Location = Model.get('stock.location')
    >>> production_loc, = Location.find([('type', '=', 'production')])
    >>> warehouse_loc, = Location.find([('code', '=', 'WH')])


Create an unit load with product in kilograms::

    >>> UnitLoad = Model.get('stock.unit_load')
    >>> unit_load = UnitLoad()
    >>> unit_load.warehouse = warehouse_loc
    >>> unit_load.production_type = 'location'
    >>> unit_load.production_location = production_loc
    >>> unit_load.product = product1
    >>> unit_load.cases_quantity = 5
    >>> unit_load.save()
    >>> unit_load.quantity = Decimal('35.0')
    >>> unit_load.tare = Decimal('2.0')
    >>> unit_load.gross_weight = Decimal('35.0')
    >>> unit_load.click('assign')
    >>> unit_load.click('do')


Weigh unit load wizard::

    >>> weigh_unit_load = Wizard('stock.unit_load.weigh')
    >>> weigh_unit_load.form.ul_code = unit_load.code
    >>> weigh_unit_load.execute('pre_data')
    >>> weigh_unit_load.form.gross_weight = Decimal('1000.0')
    >>> weigh_unit_load.form.tare = Decimal('50.0')
    >>> weigh_unit_load.execute('do_')
    >>> unit_load.reload()
    >>> unit_load.gross_weight
    1000.0
    >>> unit_load.tare
    50.0
    >>> unit_load.net_weight
    950.0
    >>> unit_load.net_weight == unit_load.quantity
    True
    >>> unit_load.quantity == sum([m.quantity for m in
    ...     unit_load.moves if m.product == unit_load.product])
    True


Create an unit load with product in units::

    >>> unit_load = UnitLoad()
    >>> unit_load.warehouse = warehouse_loc
    >>> unit_load.production_type = 'location'
    >>> unit_load.production_location = production_loc
    >>> unit_load.product = product2
    >>> unit_load.cases_quantity = 5
    >>> unit_load.quantity = Decimal('40.0')
    >>> unit_load.tare = Decimal('2.0')
    >>> unit_load.gross_weight = Decimal('35.0')
    >>> unit_load.save()


Execute wizard from unit load::

    >>> weigh_unit_load = Wizard('stock.unit_load.weigh', [unit_load])
    >>> weigh_unit_load.form.gross_weight = Decimal('980.0')
    >>> weigh_unit_load.execute('do_')
    >>> unit_load.reload()
    >>> unit_load.gross_weight
    980.0
    >>> unit_load.net_weight
    978.0
    >>> unit_load.quantity
    40.0
    >>> unit_load.quantity == sum([m.quantity for m in
    ...     unit_load.moves if m.product == unit_load.product])
    True