========================
Check unit load weight
========================

Imports::

    >>> import datetime
    >>> from trytond.tests.tools import activate_modules
    >>> from proteus import Model, Wizard
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> today = datetime.date.today()
    >>> tomorrow = today + relativedelta(days=1)


Install unit load Module::

    >>> config = activate_modules('stock_unit_load_weight')


Create company::

    >>> _ = create_company()
    >>> company = get_company()


Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> kg, = ProductUom.find([('name', '=', 'Kilogram')])
    >>> template = ProductTemplate()
    >>> template.name = 'Product'
    >>> template.default_uom = kg
    >>> template.type = 'goods'
    >>> template.list_price = Decimal('20')
    >>> template.save()
    >>> product1, = template.products
    >>> product1.cost_price = Decimal('8')
    >>> product1.save()
    >>> template = ProductTemplate()
    >>> template.name = 'Product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.list_price = Decimal('20')
    >>> template.save()
    >>> product2, = template.products
    >>> product2.cost_price = Decimal('8')
    >>> product2.save()


Get stock locations::

    >>> Location = Model.get('stock.location')
    >>> production_loc, = Location.find([('type', '=', 'production')])
    >>> warehouse_loc, = Location.find([('code', '=', 'WH')])


Create an unit load with product in kilograms::

    >>> UnitLoad = Model.get('stock.unit_load')
    >>> unit_load = UnitLoad()
    >>> unit_load.warehouse = warehouse_loc
    >>> unit_load.production_type = 'location'
    >>> unit_load.production_location = production_loc
    >>> unit_load.product = product1
    >>> unit_load.cases_quantity = 5
    >>> unit_load.save()
    >>> unit_load.quantity = Decimal('35.0')
    >>> unit_load.uom.rec_name
    'Kilogram'
    >>> unit_load.net_weight
    35.0
    >>> unit_load.tare = Decimal('2.0')
    >>> unit_load.gross_weight = Decimal('35.0')
    >>> unit_load.net_weight
    33.0
    >>> unit_load.quantity
    33.0
    >>> unit_load.quantity = Decimal('40.0')
    >>> unit_load.net_weight
    40.0
    >>> unit_load.tare
    2.0
    >>> unit_load.gross_weight
    42.0
    >>> unit_load.save()


Weigh unit load wizard::

    >>> weigh_unit_load = Wizard('stock.unit_load.weigh')
    >>> weigh_unit_load.form.ul_code = unit_load.code
    >>> weigh_unit_load.form.unit_load.code == weigh_unit_load.form.ul_code
    True
    >>> weigh_unit_load.execute('pre_data')
    >>> weigh_unit_load.form.gross_weight = Decimal('1000.0')
    >>> weigh_unit_load.form.tare = Decimal('50.0')
    >>> weigh_unit_load.execute('do_')
    >>> unit_load.reload()
    >>> unit_load.gross_weight
    1000.0
    >>> unit_load.tare
    50.0
    >>> unit_load.net_weight
    950.0
    >>> unit_load.net_weight == unit_load.quantity
    True
    >>> unit_load.quantity == sum([m.quantity for m in
    ...     unit_load.moves if m.product == unit_load.product])
    True


Execute wizard with a "Done" unit load::

    >>> unit_load.click('assign')
    >>> unit_load.click('do')
    >>> weigh_unit_load = Wizard('stock.unit_load.weigh')
    >>> weigh_unit_load.form.ul_code = unit_load.code
    >>> weigh_unit_load.execute('pre_data')
    >>> weigh_unit_load.form.gross_weight = Decimal('1020.0')
    >>> weigh_unit_load.form.tare = Decimal('50.0')
    >>> weigh_unit_load.execute('do_') # doctest: +ELLIPSIS
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: Unit load "..." is done. - 


Create an unit load with product in units::

    >>> unit_load = UnitLoad()
    >>> unit_load.start_date -= relativedelta(days=30)
    >>> unit_load.end_date -= relativedelta(days=20)
    >>> unit_load.warehouse = warehouse_loc
    >>> unit_load.production_type = 'location'
    >>> unit_load.production_location = production_loc
    >>> unit_load.product = product2
    >>> unit_load.cases_quantity = 5
    >>> unit_load.save()
    >>> unit_load.quantity = Decimal('40.0')
    >>> unit_load.uom.rec_name
    'Unit'
    >>> not unit_load.net_weight
    True
    >>> unit_load.tare = Decimal('-2.0')
    >>> unit_load.gross_weight = Decimal('35.0')
    >>> unit_load.save()
    Traceback (most recent call last):
        ...
    trytond.model.modelstorage.DomainValidationError: The value for field "Tare" in "Unit load" is not valid according to its domain. - 
    >>> unit_load.tare = Decimal('36.0')
    >>> unit_load.save()
    Traceback (most recent call last):
        ...
    trytond.model.modelstorage.DomainValidationError: The value for field "Gross weight" in "Unit load" is not valid according to its domain. - 
    >>> unit_load.tare = Decimal('2.0')
    >>> unit_load.net_weight
    33.0
    >>> unit_load.net_weight
    33.0
    >>> unit_load.tare
    2.0
    >>> unit_load.gross_weight
    35.0
    >>> unit_load.save()
    >>> unit_load.quantity
    40.0
    >>> unit_load.quantity == sum([m.quantity for m in
    ...     unit_load.moves if m.product == unit_load.product])
    True


Execute label report from wizard::

    >>> weigh_unit_load = Wizard('stock.unit_load.weigh')
    >>> weigh_unit_load.form.ul_code = unit_load.code
    >>> weigh_unit_load.execute('pre_data')
    >>> weigh_unit_load.form.gross_weight = Decimal('1000.0')
    >>> weigh_unit_load.form.tare = Decimal('50.0')
    >>> weigh_unit_load.execute('print_')
    >>> (_, _, _, _), = weigh_unit_load.actions


Execute wizard from unit load::

    >>> weigh_unit_load = Wizard('stock.unit_load.weigh', [unit_load])
    >>> weigh_unit_load.form.gross_weight = Decimal('980.0')
    >>> weigh_unit_load.execute('do_')
    >>> unit_load.reload()
    >>> unit_load.gross_weight
    980.0
    >>> unit_load.net_weight
    930.0
    >>> unit_load.quantity
    40.0
    >>> unit_load.quantity == sum([m.quantity for m in
    ...     unit_load.moves if m.product == unit_load.product])
    True


Check not available ul (dropped)::

    >>> unit_load.click('assign')
    >>> unit_load.click('do')
    >>> drop_ul = Wizard('stock.unit_load.do_drop', [unit_load])
    >>> drop_ul.form.start_date = datetime.datetime.now() - relativedelta(minutes=10)
    >>> drop_ul.form.end_date = datetime.datetime.now() - relativedelta(minutes=5)
    >>> drop_ul.form.location = production_loc
    >>> drop_ul.execute('try_')
    >>> unit_load.click('assign')
    >>> unit_load.click('do')
    >>> unit_load.reload()
    >>> bool(unit_load.available)
    False
    >>> weigh_unit_load = Wizard('stock.unit_load.weigh')
    >>> weigh_unit_load.form.ul_code = unit_load.code
    >>> weigh_unit_load.execute('pre_data') # doctest: +ELLIPSIS
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: Unit load "..." is not available. - 