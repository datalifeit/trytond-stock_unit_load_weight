# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta, Pool


class TransportReportMixin(object):

    @classmethod
    def product_weight(cls, product_key, origins, language):
        pool = Pool()
        Uom = pool.get('product.uom')
        Modeldata = pool.get('ir.model.data')

        cat_weight = Modeldata.get_id('product', 'uom_cat_weight')
        kg_uom = Uom(Modeldata.get_id('product', 'uom_kilogram'))

        res = 0
        if origins[0].__name__ != 'stock.unit_load':
            return super().product_weight(product_key, origins, language)

        product = origins[0].product
        for ul in origins:
            uom = ul.weight_unit
            if not uom and product.default_uom.category.id == cat_weight:
                uom = ul.uom
            if not uom:
                continue
            qty = ul.gross_weight
            if not qty and product.default_uom.category.id == cat_weight:
                qty = ul.quantity
            res += Uom.compute_qty(uom, qty or 0, kg_uom) or 0
        return res


class CMR(TransportReportMixin, metaclass=PoolMeta):
    __name__ = 'carrier.load.order.cmr'


class RoadTransportNote(TransportReportMixin, metaclass=PoolMeta):
    __name__ = 'carrier.load.order.road_note'
